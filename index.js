var through = require("through2");
var dotted = require("dotted");
var marked = require("marked");

module.exports = function(property)
{
    // Normalize the values.
    property = property || "summary";

    // Create and return the pipe.
    var pipe = through.obj(
        function(file, encoding, callback)
        {
            // Retrieve the value using dotted (to allow for dotted notation).
            // If we don't have it, then skip the file.
            var value = dotted.getNested(file, property);

            if (value)
            {
                // We have a value, so format it and set the value again.
                value = marked(value);
                dotted.setNested(file, property, value);
            }

            // Callback so we can continue.
            callback(null, file);
        });

    return pipe;
}
